; -----------------------------------------------------------------------
; tprep - Preparing large texts for optimal translation by an online 
;         translator.
;
; Copyright (C) 2024  Patrice Coni  <patrice.coni-dev@yandex.com>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <https://www.gnu.org/licenses/>.
; -----------------------------------------------------------------------
;
; Required Assembler: Netwide Assembler (nasm)
; Required Package:   translate-shell
;
; -----------------------------------------------------------------------
;
; Version:            0.0.1
; Using:              ./tprep <inputfile> <outputfile>
;
; -----------------------------------------------------------------------
;

BITS 64

O_RDWR              equ     2
O_WRONLY            equ     1
O_RDONLY            equ     0

SYS_UNLINK          equ     87
SYS_CREAT           equ     85
SYS_MKDIR           equ     83
SYS_WAIT4           equ     61
SYS_EXECVE          equ     59
SYS_FORK            equ     57
SYS_DUP2            equ     33
SYS_PIPE            equ     22
SYS_LSEEK           equ     8
SYS_STAT            equ     4
SYS_CLOSE           equ     3
SYS_OPEN            equ     2
SYS_WRITE           equ     1
SYS_READ            equ     0

SECTION .data
    counter         dq      0, 0x0
    offset          dq      0, 0x0
    maxchar         dd      1500, 0x0   ; max characters for processing (default value: 1500)

    msgtma          db      'Too many arguments.', 0x0
    msgs            db      'Process completed successfully.', 0x0
    msgerr          db      'An error was detected. The application must be closed.', 0x0
    msgwip          db      'In progress...', 0x0
    msgni           db      'No input.', 0x0

    ; command for translate-shell with arguments
    translatorbin   db      '/usr/bin/trans', 0x0
    argna           db      '-no-auto', 0x0
    argb            db      '-b', 0x0
    argf            db      '-f', 0x0
    argsl           db      '00', 0x0       ; string for the source language
    argt            db      '-t', 0x0
    argdl           db      '00', 0x0       ; string for the destination language
    argi            db      '-i', 0x0
    tempfilename    db      '_tmp.txt', 0x0
    cmdargs         dq      translatorbin, argna, argb, argf, argsl, argt, argdl, argi, tempfilename, 0x0

    outputfilename  db      'output_text.txt', 0x0      ; filename of the outputfile for default
    cfgfilename     db      'tprep.conf', 0x0           ; filename of the configuration file
    cfgdir          db      '.tprep', 0x0               ; directory, that contains the configuration file.

    ; default values for the file contents of the configuration file
    defaultconfig   db      'SOURCELANG=en', 0xA        ; default value for source language
                    db      'DESTINATIONLANG=de', 0xA   ; default value for destination language
                    db      'REMOVELINEFEEDS=TRUE', 0x0 ; remove line feeds automatically

    ; plain keywords
    keywsolang      db      'SOURCELANG', 0x0           ; keyword for source language
    keywdestlang    db      'DESTINATIONLANG', 0x0      ; keyword for destination language
    keywordrmlf     db      'REMOVELINEFEEDS', 0x0      ; keyword for remove line feeds automatically

    ; help screen
    header          db      'tprep - v0.0.1', 0x0
    lines           db      '--------------', 0xA, 0x0
    using           db      'Using: tprep <inputfile> <outputfile>', 0xA, 0x0

SECTION .bss
    fcon            resb    524288, ; reserved space for the file content of the input file
    foutcont        resb    8192,   ; reserved space for the output file content
    tmp             resb    8192,   ; reserved space for cache the output from the pipe temporary
    contentpart     resb    4096,   ; reserved space for buffering bytes in the later process
    buffer          resb    4096,   ; reserved space for buffering bytes in the later process
    stat_struct     resb    1024,   ; reserved space for SYS_STAT in later use
    infname         resb    1024,   ; reserved space for the input filename
    outfname        resb    1024,   ; reserved space for the output filename
    configpath      resb    1024,   ; reserved space for preparing the path to the location of the configuration directory and the configuration file
    cfgfilecontent  resb    64,     ; reserved space for the content of the configuration file
    substring       resb    64,     ; reserved space for later use in function readcfg
    valremovelf     resb    15,     ; reserved space for the value of REMOVELINEFEEDS from the configuration file

    ; reserved space for using the pipe
    fds:
    fd1             resd    1,      ; fd1 = place for read end of the pipe
    fd2             resd    1,      ; fd2 = place for write end of the pipe

SECTION .text
global _start
_start:

    pop     rcx                 ; the first value on stack is the number of arguments
    dec     rcx                 ; decrement RCX because the first value where not used
    pop     rax                 ; take the name of the application from the stack (not used)

    cmp     rcx, 0              ; are there no argument?
    je      showhlpscrn         ; show help screen if there is no argument

    cmp     rcx, 1              ; are there only one argument?
    je      oneargument

    cmp     rcx, 2              ; are there two arguments?
    je      twoarguments
    ja      errtma              ; print error message and quit program

oneargument:                    ; if only one argument was entered
    pop     r8                  ; value of input-filename to r8
    jmp     getinputfilename

twoarguments:                   ; if two arguments are entered
    pop     r8                  ; value of input-filename to r8
    pop     r10                 ; value of output-filename to r10

getinputfilename:               ; get input filename
    cmp     r8, 0x0             ; are there bytes?
    jne     wrinputfilename

wrinputfilename:                ; write input-filename to memory
    mov     [infname], rsi

setoutputfilename:              ; set output-filename
    cmp     r10, 0x0            ; are there bytes?
    jne     wroutfilename

noin:                           ; if there is no user input, set standard filename
    mov     rsi, outputfilename

wroutfilename:                  ; write output-filename to memory
    mov     [outfname], rsi

nextvar:                        ; take user home directory variable from the environment variables from the Stack
    pop     rax
    cmp     rax, 4              ; the content must be at least 4 bytes in size. Otherwise there will be a crash.
    jb      nextvar
    cmp     DWORD [rax], 'HOME' ; The keyword for it contains 'HOME'
    jne     nextvar

    lea     rdx, [rax+5]        ; move only home path to rdx and set offset to 5 to move it without 'HOME='
    mov     rax, rdx

    call    setcfgfile

    mov     rdi, configpath     ; set filename of the configuration file
    mov     rax, SYS_OPEN       ; invoke SYS_OPEN (kernel opcode 2)
    mov     rsi, 0              ; set flag for readonly access mode
    mov     rdx, O_RDONLY       ; mode value not used
    syscall

    mov     rdi, rax            ; the operand file descriptor is in rax after SYS_OPEN was running
    mov     rax, SYS_READ       ; invoke SYS_READ (kernel opcode 0)
    mov     rsi, cfgfilecontent ; set memory address for the file contents variable
    mov     rdx, 64             ; sets number of bytes to read
    syscall

    mov     rax, SYS_CLOSE      ; invoke SYS_CLOSE (kernel opcode 3) to close the file
    mov     rdi, rdi            ; keep the same file descriptor
    syscall

    mov     r10, cfgfilecontent ; read the value for the source language from configuration file
    mov     r8, keywsolang
    call    readcfg
    cmp     rax, -1             ; is there an error?
    je      errorprog
    mov     rdx, rax            ; move string to rdx to use rax for counting the characters
    call    slen                ; get the number of characters
    mov     rsi, rdx            ; copy the string to memory placed labled argsl
    mov     rdi, argsl
    mov     rcx, rax
    cld
    rep     movsb

    mov     r10, cfgfilecontent ; read the value for the destination language from configuration file
    mov     r8, keywdestlang
    call    readcfg
    cmp     rax, -1             ; is there an error?
    je      errorprog
    mov     rdx, rax            ; move string to rdx to use rax for counting the characters
    call    slen                ; get the number of characters
    mov     rsi, rdx            ; copy the string to memory placed labled argdl
    mov     rdi, argdl
    mov     rcx, rax
    cld
    rep     movsb

    mov     r10, cfgfilecontent ; read the value for for automatic linefeed remove from configuration file
    mov     r8, keywordrmlf
    call    readcfg
    cmp     rax, -1             ; is there an error?
    je      errorprog
    mov     rdx, rax            ; move string to rdx to use rax for counting the characters
    call    slen                ; get the number of characters
    mov     rsi, rdx            ; copy the string to memory placed labled valremovelf
    mov     rdi, valremovelf
    mov     rcx, rax
    cld
    rep     movsb

    call    getfilecontent      ; read the content of the file and cache it

    mov     rax, fcon           ; check if the file content is empty
    call    slen
    cmp     rax, 0x0
    je      noinput

    mov     rax, 0              ; set offset from where the copy of the string starts
    mov     [offset], rax

    mov     rax, msgwip         ; print message "In progress..."
    call    sprintLF

mloop:
    mov     rax, [valremovelf]
    cmp     rax, 'TRUE'         ; Remove linefeeds automatically?
    jne     doproc              ; if not, line breaks will be remain
    call    replaceLF

doproc:
    mov     rcx, 8192           ; overwrite memory labled tmp with zeros
    mov     rax, 0
    mov     rdi, tmp
    rep     stosb

    mov     rcx, 8192           ; overwrite memory labled contentpart with zeros
    mov     rax, 0
    mov     rdi, foutcont
    rep     stosb

    mov     rcx, 4096           ; overwrite memory labled buffer with zeros
    mov     rax, 0
    mov     rdi, buffer
    rep     stosb

    mov     rcx, 4096           ; overwrite memory labled contentpart with zeros
    mov     rax, 0
    mov     rdi, contentpart
    rep     stosb

    mov     rcx, [offset]       ; copy string part (n maxchar) to memory labled buffer
    lea     rsi, [fcon+rcx]     ; set offset from where copying starts.
    mov     rcx, [maxchar]      ; define maximum number of characters to be copied
    mov     rdi, buffer         ; set pointer to place in memory labled buffer where the substring is copied to
    cld
    rep     movsb

    call    sprepare
    call    stfile

    mov     rax, SYS_PIPE       ; make pipe
    mov     rdi, fds
    syscall
    cmp     rax, 0x0            ; check that there is no error
    jnz     errorprog

    mov     rax, SYS_FORK       ; fork process
    syscall
    cmp     rax, 0              ; the child process returns 0
    jz      child

    mov     rdi, rax            ; wait until the child process is finished by invoking SYS_WAIT4
    mov     rax, SYS_WAIT4
    xor     rsi, rsi
    xor     rdx, rdx
    xor     r10, r10
    syscall

    mov     rax, 3              ; close fd2 because parent process does read only from pipe
    mov     rdi, [fd2]
    syscall

    mov     rax, 0              ; read from fd1 and write content to tmp
    mov     rsi, tmp
    mov     rdi, [fd1]
    mov     rdx, 524288
    syscall

    call    sotf                ; save output to file

    mov     rax, [counter]      ; set new offset for next stringpart
    add     rax, [offset]
    mov     [offset], rax

    lea     rdx, [fcon+rax]     ; will check, if the end of the string has been achieved.
    cmp     byte [rdx], 0
    jnz     mloop

    call    deltmpfile          ; delete temp-file because is no longer used

    mov     rax, msgs
    call    sprintLF

    call    quit

child:
    mov     rax, SYS_CLOSE      ; close fd1 because child process does write only to pipe
    mov     rdi, [fd1]
    syscall

    mov     rax, SYS_DUP2       ; delete close-on-exec flag and provide the duplicated filedescriptor
    mov     rdi, [fd2]
    mov     rsi, 1
    syscall

    mov     rax, SYS_CLOSE      ; close fd2 because is no longer needed
    mov     rdi, [fd2]
    syscall

    mov     rax, SYS_EXECVE     ; invoke translate-shell
    mov     rdi, translatorbin
    mov     rsi, cmdargs
    xor     rdx, rdx
    syscall

noinput:                        ; if there is no content, display message and quit the program
    mov     rax, msgni
    call    sprintLF
    call    quit

showhlpscrn:                    ; display help screen
    mov     rax, header
    call    sprintLF            ; display header

    mov     rax, lines
    call    sprintLF            ; display lines

    mov     rax, using
    call    sprintLF            ; display using string

    call    quit

errtma:                         ; if there are too many arguments
    mov     rax, msgtma
    call    sprintLF
    call    quit

errorprog:                      ; if there is a fatal error
    mov     rax, msgerr
    call    sprintLF
    call    quit

deltmpfile:                     ; function to delete the temp-file
    mov     rdi, tempfilename
    mov     rax, SYS_UNLINK
    syscall
    ret

; ----------------------------------------------------------
; Function to save the output from pipe, to a file.
; If file exists, update the existing file with the new
; content.
; ----------------------------------------------------------
sotf:
    push    rax                 ; push the values of the registers onto the stack to preserve it
    push    rdx
    push    rdi
    push    rsi
    push    rcx

    mov     rax, SYS_OPEN       ; open the file with readonly permission
    mov     rdi, [outfname]
    mov     rdx, O_RDONLY
    mov     rsi, 0
    syscall

    mov     rdi, rax            ; read from file
    mov     rax, SYS_READ
    mov     rsi, foutcont
    mov     rdx, 524288
    syscall

    mov      rax, SYS_CLOSE     ; remove the active file descriptor
    mov      rdi, rdi
    syscall

    mov     rax, foutcont       ; check if the file is empty/exists
    call    slen
    cmp     rax, 0x0
    jne     .next

    mov     rax, SYS_CREAT      ; if no content, then create a new file
    mov     rdi, [outfname]
    mov     rsi, 0777o
    syscall

.next:
    mov     rdi, [outfname]     ; open file with write-only permissions
    mov     rax, 2
    mov     rsi, O_WRONLY
    syscall

    mov     rdi, rax            ; reposition file offset to the end of the file
    mov     rax, SYS_LSEEK
    mov     rdx, 2
    mov     rsi, 0
    syscall

    mov     rsi, tmp            ; write content to file
    mov     rax, rsi
    call    slen
    mov     rdx, rax
    mov     rdi, rdi
    mov     rax, SYS_WRITE
    syscall

    mov     rax, SYS_CLOSE      ; close file
    mov     rdi, rdi
    syscall

    pop     rcx                 ; restore the values in the registers, bevore the function was started.
    pop     rsi
    pop     rdi
    pop     rdx
    pop     rax
    ret

; ----------------------------------------------------------
; Function to save the contentpart, that is in memory
; cached to a temp-file.
; ----------------------------------------------------------
stfile:
    push    rax                 ; push the values of the registers onto the stack to preserve it
    push    rdx
    push    rdi
    push    rsi

    mov     rax, tempfilename   ; does file exist?
    mov     [infname], rax
    call    getfilecontent
    xor     rdx, rdx
    mov     rax, [fcon]
    cmp     rax, rdx            ; if file exists, delete it
    je      .create

    call    deltmpfile          ; delete temp-file

.create:                        ; create file
    mov     rax, tempfilename
    mov     [infname], rax

    mov     rax, SYS_CREAT
    mov     rdi, [infname]
    mov     rsi, 0777o
    syscall

.write:                         ; write the content to the file
    mov     rdi, rax
    mov     rax, SYS_WRITE
    mov     rdx, [counter]
    mov     rsi, contentpart
    syscall

    mov      rax, SYS_CLOSE     ; close file
    mov      rdi, rdi
    syscall

    pop     rsi                 ; restore the values in the registers, bevore the function was started.
    pop     rdi
    pop     rdx
    pop     rax
    ret

; ----------------------------------------------------------
; Function to read content of the input-file
; ----------------------------------------------------------
getfilecontent:
    mov     rax, SYS_OPEN
    mov     rdi, [infname]
    mov     rdx, O_RDONLY
    mov     rsi, 0
    syscall

    mov     rdi, rax
    mov     rax, SYS_READ
    mov     rsi, fcon
    mov     rdx, 524288
    syscall

    mov     rax, SYS_CLOSE
    mov     rdi, rdi
    syscall
    ret

; ----------------------------------------------------------
; Function to replace line feeds with space characters
; ----------------------------------------------------------
replaceLF:
    push    r9                  ; push the values of the registers onto the stack to preserve it
    push    rax
    push    rcx
    push    rdx
    push    rsi

    xor     rsi, rsi
    xor     rax, rax
    xor     rcx, rcx
    xor     rdx, rdx
    xor     r9, r9

    mov     r9, fcon            ; point to memory place containing the file contents string
    mov     rax, r9
    call    slen                ; get the number of characters of the string
    mov     rcx, rax            ; init rcx
    xor     rax, rax
    call    .scan
    jmp     .end

.repchar:
    mov     byte [r9+rax], 0x20

.scan:
    movzx   rdx, byte [r9+rax]
    cmp     rdx, 0x0A
    je      .repchar
    inc     rax
    loop    .scan
    ret

.end:
    pop     rsi                 ; restore the values in the registers, bevore the function was started.
    pop     rdx
    pop     rcx
    pop     rax
    pop     r9
    ret

; ----------------------------------------------------------
; Function to detect the dot at the end of last sentence in
; location "buffer" and save the location of it as offset.
; If no point was found, then the string is copied and the
; lengh of the string is the offset.
; ----------------------------------------------------------
sprepare:
    push    rax                 ; push the values of the registers onto the stack to preserve it
    push    rdx
    push    rcx
    push    rdi
    push    rsi

    xor     rax, rax            ; init registers
    xor     rdx, rdx
    xor     rcx, rcx
    xor     rdi, rdi
    xor     rsi, rsi

    mov     rdx, buffer

    mov     rcx, [maxchar]      ; does the string contain a "dot" character?
    mov     al, 0x2E            ; the dot is the character to be searched for
    mov     rdi, rdx
    cld
    repne   scasb
    jnz     .nodot
    jz      .wdot

.wdot:                          ; count n maxchar down to the last dot character in the string
    movzx   rax, byte [rdx+rcx] ; move one byte, at the position, pointed by the counter, to rax
    cmp     rax, 0x2E           ; it's the character a dot?
    je      .split              ; if yes, split the string at this position there
    dec     rcx                 ; decrement the counter for comparing next character during the next loop
    jmp     .wdot               ; repeat until the dot was found

.nodot:                         ; store the maximum number of characters in memory if no dot was found
    mov     rax, rdx            ; move content to rax, for counting characters of the string
    call    slen                ; invoke slen to get the number of characters of the string
    mov     rcx, rax            ; init counter
    mov     [counter], rcx      ; move value into memory place labled counter for later use
    jmp     .movsp              ; copy the full stringpart to the memory

.split:
    inc     rcx                 ; increment rcx to include the dot a the end of the last sentence
    mov     [counter], rcx      ; move value into memory place labled counter for later use

.movsp:                         ; write stringpart into the memory
    mov     rsi, buffer         ; the source is the place labled buffer in memory
    mov     rdi, contentpart    ; point to place in the memory labled contentpart to set the destination
    cld                         ; make the operation left to right
    rep     movsb               ; repeat until the counter is zero

.end:
    pop     rsi                 ; restore the values in the registers, bevore the function was started.
    pop     rdi
    pop     rcx
    pop     rdx
    pop     rax
    ret

; ----------------------------------------------------------
; Function to print a string and will add a line feed
; at the end.
; ----------------------------------------------------------
sprintLF:
    call    sprint

    push    rax
    mov     rax, 0x0A
    push    rax
    mov     rax, rsp            ; move the address of the current stack pointer to RAX for sprint
    call    sprint
    pop     rax                 ; remove the linefeed character from the stack
    pop     rax                 ; restore the original value of RAX bevore this function is runned
    ret

; ----------------------------------------------------------
; Function to print a string
; ----------------------------------------------------------
sprint:
    push    rdx
    push    rcx
    push    rbx
    push    rax

    xor     rdi, rdi
    xor     rsi, rsi

    call    slen

    mov     rdx, rax
    pop     rsi

    mov     rcx, rax
    mov     rbx, 1
    mov     rax, 1
    syscall

    pop     rbx
    pop     rcx
    pop     rdx
    ret

; ----------------------------------------------------------
; Function to calculate the string lengh
; ----------------------------------------------------------
slen:
    push    rbx
    mov     rbx, rax

.nextchar:
    cmp     byte [rax], 0x0
    jz      .finished
    inc     rax
    jmp     .nextchar

.finished:
    sub     rax, rbx
    pop     rbx
    ret

; ----------------------------------------------------------
; Function to check if the directory, in which the config
; file is saved and does create it, if there not exist. Then
; it checks if the config file in this directory does exist,
; and create a new config file, if does not exist.
;
; Default path is: ~/.tprep
; ----------------------------------------------------------
setcfgfile:
    push    rsi                     ; push the values of the registers onto the stack to preserve it
    push    rdi
    push    rcx
    push    rbx

.countchar:                         ; count the number of characters in the path
    cmp     byte [rax], 0x0
    jz      .preparepath
    inc     rax
    jmp     .countchar

.preparepath:
    sub     rax, rdx                ; substract rax from rdx to get the number of characters
    inc     rax                     ; increment rax to set the slash character later
    push    rax                     ; preserve the value on the stack for later
    mov     byte [rdx+rax-1], 0x2F  ; add a slash character at the end of the string

    mov     rcx, rax                ; initialize counter to copy the home path string
    mov     rsi, rdx
    mov     rdi, configpath
    cld
    rep     movsb                   ; copy the current home path from the environment variable to the memory

    mov     rdx, cfgdir             ; next one: Add the dirname to the dirpath string
    mov     rax, rdx

.countdirname:                      ; count the number of characters from the directory name
    cmp     byte [rax], 0x0
    jz      .createfullpath
    inc     rax
    jmp     .countdirname

.createfullpath:                    ; Completes the full path
    sub     rax, rdx                ; substract rax from rdx to get the number of characters
    mov     rbx, rax                ; keep value to push it later to the stack for preserve it

    mov     rcx, rax                ; initialize counter to copy the directory name string
    pop     rax                     ; take the value number of characters from the homepath from the stack
    mov     rsi, cfgdir
    lea     rdi, [configpath+rax]   ; The value of RAX is the offset to not overwrite the home path
    cld
    rep     movsb                   ; copy the directory name to the position after the home path

    push    rbx                     ; preserve the number of characters of the directory name for later
    push    rax                     ; preserve the number of characters of the home path for later

.checkifexists:                     ; checks if the directory exists
    mov     rax, SYS_STAT           ; invoke SYS_STAT (kernel opcode 4) to check if the directory exists or not
    mov     rsi, stat_struct
    mov     rdi, configpath
    syscall                         ; if directory exists returns -2, otherwise it returns 0

    cmp     rax, 0x0                ; does the directory exist?
    je      .next

    mov     rax, SYS_MKDIR          ; if the directory does not exist, create it
    mov     rdi, configpath
    mov     rsi, 0777o
    syscall

.next:
    mov     rdx, cfgfilename
    mov     rax, rdx

.countfilename:                     ; counts the number of characters in the filename
    cmp     byte [rax], 0x0
    jz      .preparefullpath
    inc     rax
    jmp     .countfilename

.preparefullpath:                   ; prepare the file path with the config filename
    sub     rax, rdx
    mov     rcx, rax                ; init rcx to copy the string of the filename

    pop     rax                     ; take the number of characters of the directory from the stack
    pop     rbx                     ; take the number of characters of the home path from the stack
    add     rax, rbx                ; add the numbers to set the offset value to prepare the full path that contains the filename
    inc     rax                     ; increment rax to set a slash at the end of the string

    mov     rdx, configpath
    mov     byte [rdx+rax-1], 0x2F  ; add a slash character at the end of the string
    mov     rsi, cfgfilename
    lea     rdi, [configpath+rax]   ; set offset value in RAX to add the filename at the end of the path
    cld
    rep     movsb                   ; copy the string of the filename at the end of the config directory path of the application

.createcfgfile:
    mov     rax, SYS_STAT           ; invoke SYS_STAT to check if the file exists or not
    mov     rsi, stat_struct
    mov     rdi, configpath
    syscall                         ; if file exists returns -2, otherwise it returns 0

    cmp     rax, 0x0                ; does the file exists?
    je      .finish

    mov     rax, defaultconfig      ; count number of characters from string removelinefeeds and add it to rdx
    call    slen
    mov     rdx, rax

    mov     rax, SYS_CREAT          ; if no content, then create a new file
    mov     rdi, configpath
    mov     rsi, 0777o
    syscall

    mov     rdi, rax                ; write default configuration to the file
    mov     rax, SYS_WRITE
    mov     rdx, rdx
    mov     rsi, defaultconfig
    syscall

    mov     rax, SYS_CLOSE          ; close file
    mov     rdi, rdi
    syscall

.finish:
    pop     rbx                     ; restore the values in the registers, bevore the function was started.
    pop     rcx
    pop     rdi
    pop     rsi
    ret

; ----------------------------------------------------------
; Function to return a value from a keyword in a config
; file. The value is returned in register RAX. If -1 is
; returned in register RAX, then there is an error.
;
; Required:
;   * In register R10 is the string containing the content
;     of the config file required.
;   * In register R8 is the string containing the keyword
;     required.
;   * Reserved memory in which the substring can be stored.
; ----------------------------------------------------------
readcfg:
    push    rcx                     ; push the values of the registers onto the stack to preserve it
    push    rbx
    push    rsi
    push    rdi
    push    rdx

    xor     rbx, rbx                ; init registers
    xor     rcx, rcx
    xor     rax, rax
    xor     rdi, rdi
    xor     rsi, rsi
    xor     rdx, rdx

.nextline:
    mov     rcx, 64                 ; overwrite memory labled substring with zeros
    mov     rax, 0
    mov     rdi, substring
    rep     stosb

    lea     r9, [r10 + rdx]         ; the line that is processed is always at the top. The offset is in RDX.
    xor     rbx, rbx                ; init rbx
    cmp     byte [r9 + rbx], 0x0    ; has the end of the file contents been reached?
    je      .error                  ; if yes, the keyword was not found

.countchars:
    cmp     byte [r9 + rbx], 0xA    ; has the end of the line been achieved? The line feed is the delimiter
    je      .extractstring
    cmp     byte [r9 + rbx], 0x0    ; has the end of the file been achieved? The zero is now the delimiter
    je      .extractstring
    inc     rbx                     ; increment offset value
    jmp     .countchars

.extractstring:                     ; extracts the current line from the file contents string
    push    rbx                     ; push the number of the characters in the line that was read to the stack
    push    rdx                     ; push the offset value to the stack

    mov     rcx, rbx                ; set counter
    mov     rsi, r9
    mov     rdi, substring          ; transfer the line to the memory location labled substring
    cld                             ; set flag to transfer characters from right to left
    rep     movsb

    mov     rax, r8                 ; move pointer to the keyword string to rax for counting characters in the next step

.countkw:                           ; count the bytes/characters of the keyword (0x0 is the delimiter)
    cmp     byte [rax], 0x0
    jz      .substract
    inc     rax
    jmp     .countkw

.substract:
    sub     rax, r8                 ; the difference is the number of the character from the keyword

.checkkeyword:                      ; checks whether it is the keyword where are looking for
    mov     rcx, rax
    mov     rdi, substring
    mov     rsi, r8
    cld                             ; set flag to count from right to left
    repe    cmpsb                   ; is the keyword included?
    je      .extractvalue           ; if yes, then extract the value from the substring

    pop     rdx                     ; take the offset value from the stack
    pop     rcx                     ; take the number of characters from the substring from the stack
    inc     rcx                     ; increment rcx to include the linefeed in the offset value
    add     rdx, rcx                ; add the characters of the line to the offset value in rdx
    jmp     .nextline               ; read the next line

.extractvalue:                      ; if the line contains the keyword, then extract the value
    lea     rax, [rdi + 1]          ; extract the value from the string containing the line. Add +1 to skip the '=' character

    pop     rdx                     ; the offset value is no longer used
    pop     rdx                     ; the number of characters from the substring is no longer used

    jmp     .return

.error:
    mov     rax, -1                 ; if no keyword was found, the return -1 in RAX

.return:
    pop     rdx                     ; restore the values in the registers, bevore the function was started.
    pop     rdi
    pop     rsi
    pop     rbx
    pop     rcx
    ret

; ----------------------------------------------------------
; Exit program
; ----------------------------------------------------------
quit:
    xor     rdi, rdi
    mov     rax, 60
    syscall
    ret
