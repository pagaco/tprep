FILENAME=tprep
PREFIX=/usr

all: 	clean
	mkdir bin
	nasm -f elf64 src/$(FILENAME).asm
	ld src/$(FILENAME).o -o bin/$(FILENAME)
	rm -f src/$(FILENAME).o

clean:
	rm -rf bin
	rm -f $(FILENAME).o
	rm -f $(FILENAME)

install:
	install bin/$(FILENAME) $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/$(FILENAME)
