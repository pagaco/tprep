# Intodruction

Tprep is a simple application, compatible with 64-bit Linux operating systems, written in nasm  (Netwide Assembler), for preparing/structuring texts to be translated using an online translator. It allows an optimal translation of large texts by an online translator. 

## Required packages

Tprep does structuring large texts and forwards them in a second step automatically to _translate-shell_. The installation of _translate-shell_ is required.

## Build and Installation

### Required package:

- nasm (Netwide Assembler)

### Build and Installation

```
git clone https://gitlab.com/pagaco/tprep
cd tprep
make
sudo make install
```

## Using

On the first run, tprep creates a directory _(~/.tprep)_ and in a second step the configuration file  _(~/.tprep/tprep.conf)_. The configuration file contains variables to set the source language, the destination language and whether linebreaks should be removed. The application does write the output of _translate-shell_ in a text file.

### Synopsis
`tprep <inputfile> <outputfile>`

## License
GPLv3
